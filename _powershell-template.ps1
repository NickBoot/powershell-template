<#
.SYNOPSIS
    The synopsis goes here. This can be one line, or many.
.DESCRIPTION
    The description is usually a longer, more detailed explanation of what the script or function does. Take as many lines as you need.
.PARAMETER computername
    Here, the dotted keyword is followed by a single parameter name. Don't precede that with a hyphen. The following lines describe the purpose of the parameter:
.PARAMETER filePath
    Provide a PARAMETER section for each parameter that your script or function accepts.
.EXAMPLE
    There's no need to number your examples.
.EXAMPLE
    PowerShell will number them for you when it displays your help text to a user.
.NOTES
    Version:            0.1
    Author:             <Author>
    Creation Date:      01-01-2021
    Edit Date:          02-01-2021
    Author company:     <Author company>
    Customer/Company:   <Customer name>
    Template version:   2.0
    ===============================================
    Os tested:          Windows 10, Windows Server 2016
    PowerShell version: 7.2
    ===============================================
    Changelog:
    <          

    >
    ===============================================
    ! DISCLAIMER OF WARRANTIES:

    * THE SOFTWARE PROVIDED HEREUNDER IS PROVIDED ON AN "AS IS" BASIS, WITHOUT
    * ANY WARRANTIES OR REPRESENTATIONS EXPRESS, IMPLIED OR STATUTORY; INCLUDING,
    * WITHOUT LIMITATION, WARRANTIES OF QUALITY, PERFORMANCE, NONINFRINGEMENT,
    * MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  NOR ARE THERE ANY
    * WARRANTIES CREATED BY A COURSE OR DEALING, COURSE OF PERFORMANCE OR TRADE
    * USAGE.  FURTHERMORE, THERE ARE NO WARRANTIES THAT THE SOFTWARE WILL MEET
    * YOUR NEEDS OR BE FREE FROM ERRORS, OR THAT THE OPERATION OF THE SOFTWARE
    * WILL BE UNINTERRUPTED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
    * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    * EXEMPLARY, OR CONSEQUENTIAL DAMAGES HOWEVER CAUSED AND ON ANY THEORY OF
    * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    ===============================================
.LINK 
    https://example.com
#>

# Install Better Comments to get colourised comment
# Name: Better Comments
# VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments

#       Normal information
#*      Important information
#!      Alert information
#?      Question
#TODO   Todo information


##*===============================================
##* INITIALISATIONS
##*===============================================
Param (
	[Parameter(Mandatory=$True)]
	[System.String] $OTAP,
    [Parameter(Mandatory=$True)]
	[System.String] $Action
)


##*===============================================
##* POWERSHELL MODULES
##*===============================================
#import-module


##*===============================================
##* VARIABLE DECLARATION
##*===============================================
# Variables: Script




##*===============================================
##* SCRIPT
##*===============================================
If ($OTAP -eq 'acc')
    {
    Write-Verbose $OTAP - Run script with action
    & "$PSScriptRoot\script.ps1" -Action $Action
    }

ElseIf ($OTAP -eq 'Prod')
    {
    Write-Verbose $OTAP - Reboot computer2, computer3
    Restart-Computer -ComputerName computer2, computer3
    }

Else
    {
    Write-Verbose $OTAP is no correct action 
    Write-Verbose Please try again
    }
##*===============================================
##* END SCRIPT
##*===============================================